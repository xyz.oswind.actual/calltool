#!/usr/bin/python
# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

# XXXX wrong; export more.
__all__ = [ 'from_objdump', 'from_nm' ]

"""
   Code to use binary inspection tools to find symbol lists and callgraphs.
"""

import calltool.Callgraph
import os
import subprocess
import re
import pickle
import platform
from logging import warn
from Misc import b, u

def stdout_lines(cmd):
    """Generator: run the command 'cmd', and yield all of its stdout lines."""
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, close_fds=True)
    for line in p.stdout:
        yield line
    p.wait()

def pretty_name(fname):
    """Given the name for a .o, .so, or .c file, try to find a pretty
       human-readable name for it.  Ideally the name should be the
       name of the relevant c file, minus the '.c' extension.
    """
    fname = os.path.basename(fname)
    if ".so." in fname:
        fname = fname.split(".")[0]
    else:
        fname = os.path.splitext(fname)[0]
    m = re.match('^src_.*_a-(.*)', fname)
    if m:
        fname = m.group(1)
    return fname

def strip_sym_mac(s):
    m = re.match(b(r"R\d+\$(.*)"), s)
    if m:
        s = m.group(1)
    if s[0:1] != b('_'):
        #warn("%s didn't start with _", s)
        return u(s)
    return u(s[1:])

def strip_sym_linux(s):
    return u(s)

strip_sym = { 'Linux' : strip_sym_linux,
              'Darwin' : strip_sym_mac }[platform.system()]

def objdump_mac(objfile):
    func_label_re = re.compile(b(r'^([a-zA-Z0-9_]+):'))
    call_re = re.compile(b(r'.*(?:callq|jmp)\s+([a-zA-Z0-9_]+)'))
    for line in stdout_lines(["objdump", "-disassemble", "-macho",
                              "-indirect-symbols", objfile]):
        m = func_label_re.match(line)
        if m:
            yield ("FUNC", strip_sym(m.group(1)))
            continue
        # check for a regular call with callq
        m = call_re.match(line)
        if m and not m.group(1).startswith(b("0x")):
            yield ("CALL", strip_sym(m.group(1)))
            continue

def objdump_linux(objfile):
    func_label_re = re.compile(
        b(r'^[0-9a-fA-F]+\s+<([a-zA-Z0-9_]+)(\.[^>]+)?>:'))
    call_re = re.compile(b(r'.*(?:callq?|jmpq?).*<([^>]+)>'))
    reloc_re = re.compile(b(r'^\s+[a-fA-F0-9]+:\s+R_X86_\S+\s+([a-zA-Z0-9_]+)'))

    pending_call = False

    for line in stdout_lines(["objdump", "-rd", objfile]):
        if pending_call:
            pending_call = False
            m = reloc_re.match(line)
            if m:
                yield ("CALL", u(m.group(1)))
                continue

        m = func_label_re.match(line)
        if m:
            yield ("FUNC", u(m.group(1)))
            continue

        m = call_re.match(line)
        if m:
            target = m.group(1)
            if b("-") in target or b("+") in target:
                pending_call = True
            else:
                yield ("CALL", u(target))
            continue

objdump_generator = { 'Linux' : objdump_linux,
                      'Darwin' : objdump_mac }[platform.system()]

discard = lambda *x, **y: None

# for osx must use: -disassemble -macho -indirect-symbols
#     or maybe use: otool -tV

def from_objdump(objfile):
    """Given an object file called 'objfile', use the objdump tool to
       build a Callgraph.Module object for it.  Return that Module object.
    """

    mod = calltool.Callgraph.Module(pretty_name(objfile), fname=objfile)

    add_to_func = discard

    for kind, symbol in objdump_generator(objfile):
        if kind == "FUNC":
            funcname = symbol
            mod.addDefinition(funcname)
            add_to_func = lambda sym, isCall: mod.addUse(sym, user=funcname, isCall=isCall)
            continue
        elif kind == "CALL":
            add_to_func(symbol, True)

    return mod

# These flags mean that the symbol is defined in and exported from this
# file.
NM_DEFINED_FLAGS = "ABCDRTVW"+"is"

# These flags mean that this symbol is used by this file.
NM_USED_FLAGS = "U"

# These flags mean that the symbol exists only internally to the file
# and can safely be ignored.
NM_IGNORE_FLAGS = "Sbdrtvw"

def from_nm(objfile):
    """Given the name of a .o or .so file, use nm to find out which symbols
       it uses and which symbols it defines.  Return this information in a
       Callgraph.Module object.

       .so files are treated as 'External', so we won't actually
       follow the symbols that they use.

       Note that nm can't tell us which functions call which other
       functions, so all of the called functions in this module will
       be treated as having been called by a function called '*'.
    """
    is_so_file = (objfile.endswith(".so") or ".so." in objfile)

    flags = []
    if is_so_file:
        # You have to say "-D" for .so files.
        flags.append("-D")

    mod = calltool.Callgraph.Module(pretty_name(objfile), fname=objfile)

    flag_action = {}

    for flag in NM_DEFINED_FLAGS:
        flag_action[b(flag)]=mod.addDefinition
    for flag in NM_USED_FLAGS:
        flag_action[b(flag)]=mod.addUse
    for flag in NM_IGNORE_FLAGS:
        flag_action[b(flag)]=discard

    for line in stdout_lines(["nm"]+flags+[objfile]):
        parts = line.split()
        if len(parts) == 3:
            addr, flag, symbol = parts
        elif len(parts) == 2:
            flag, symbol = parts

        try:
            action = flag_action[flag]
        except KeyError:
            warn("nm %s return give a weird flag %s in line %r; ignoring.",
                 objfile, flag, line)
            action = flag_action[flag] = discard

        action(strip_sym(symbol))

    return mod

def from_objdump_and_nm(objfile):
    """Use objdump and nm to find all the symbols in 'objfile'.  (Since
       that objdump doesn't tell us about non-function symbols, and nm
       can't tell us about function-by-function callgraphs, we have to
       use both.)
    """
    # (We could make objdump tell us about other symbols, but it wouldn't
    # be any faster, really.)
    mod = from_objdump(objfile)
    mod_nm = from_nm(objfile)

    for sym in mod_nm.getDefinitions():
        mod.addDefinition(sym)

    return mod

def from_objdump_and_nm_cached(objfile):
    path, name = os.path.split(objfile)
    cachepath = os.path.join(path, ".%s.graph" % name)
    try:
        cache_st = os.stat(cachepath)
        obj_st = os.stat(objfile)
        if cache_st.st_mtime >= obj_st.st_mtime:
            mod = pickle.load(open(cachepath, 'rb'))
            if getattr(mod, "_version", 0) >= calltool.Callgraph.MOD_VERSION:
                return mod
    except (OSError, pickle.PickleError, EOFError, ValueError):
        pass

    mod = from_objdump_and_nm(objfile)
    pickle.dump(mod, open(cachepath, 'wb'))
    return mod


def list_shared_libraries_ldd(binary):
    for line in stdout_lines(["ldd", binary]):
        m = re.match(b(r'^\s+.*=> (\S+)'), line)
        if not m:
            continue
        yield m.group(1)

def list_shared_libraries_otool(binary):
    for line in stdout_lines(["otool", "-L", binary]):
        m = re.match(b(r'^\s+(\S+\.dylib)\s'), line)
        if not m:
            continue
        yield m.group(1)

sharedlib_generator = { 'Linux' : list_shared_libraries_ldd,
                        'Darwin' : list_shared_libraries_otool }[
                            platform.system()]

def from_shared_libraries(binary):
    """Generator: yield a Callgraph.Module for every shared library used
       by 'binary'.  (Uses ldd to find out which shared libraries
       those are.)

    """
    for library_name in sharedlib_generator(binary):
        mod = from_nm(u(library_name))
        mod.makeExternal()
        yield mod

if __name__ == '__main__':
    import sys
    from_objdump(sys.argv[1]).dump()
    print("---------")
    from_nm(sys.argv[1]).dump()
