#!/usr/bin/python

# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

"""
   Implements a set of module-by-module restrictions on a callgraph.
"""

__all__ = [ 'Rules', 'NothingAllows', 'MatchRule', 'Forbid', 'Allow',
            'RuleBuilder' ]

class Rules(object):
    """Set of rules.  Rules apply in numerically ascending priority order."""
    def __init__(self):
        self._rules = []
        self._sorted = True

    def addRule(self, rule, priority=0):
        self._rules.append((priority, rule))
        self._sorted = False

    def _getOrderedRules(self):
        if not self._sorted:
            self._rules.sort()
            self._sorted = True
        return self._rules

    def rulesAllowCall(self, cg, mod1, mod2):
        """Return a Result from trying to apply these rules to a
           call in the Callgraph cg from Module mod1 to Module mod2.
           Return None if no rule applies."""
        for priority, rule in self._getOrderedRules():
            result = rule.apply(cg, mod1, mod2)
            if result is not None:
                return result
        return None

    def violations(self, cg):
        """Generator: yield every Violation in the callgraph."""
        modcalls, _ = cg.buildModuleCalls()
        for modName1 in modcalls:
            mod1 = cg.getModule(modName1)
            for modName2 in modcalls[modName1]:
                mod2 = cg.getModule(modName2)
                result = self.rulesAllowCall(cg, mod1, mod2)
                if result.isError():
                    yield result

class Result(object):
    """Result of applying a rule to a function call."""
    def __str__(self):
        return self._msg
    def isError(self):
        return False

class OK(Result):
    """Result of applying a rule to a permitted call."""
    def __init__(self, msg, mod1, mod2):
        self._msg = msg

class Fail(Result):
    """Result of applying a rule to a fobridden call."""
    def __init__(self, msg, mod1, mod2):
        self._msg = msg
    def isError(self):
        return True

class Rule(object):
    """Base rule abstract type: allows one module to call another, or not."""
    def apply(self, cg, mod1, mod2):
        raise NotImplemented()

class NothingAllows(Rule):
    """Rule implementation: reject everything."""
    def apply(self, cg, mod1, mod2):
        return Fail("Nothing explicitly allows call", mod1, mod2)

class ItsAllOkay(Rule):
    """Rule implementation: accept everything."""
    def apply(self, cg, mod1, mod2):
        return OK("Nothing prevents call", mod1, mod2)

def do_iter(obj):
    """Return an iterator over 'obj', which is either a singleton or a
       collection."""
    if isinstance(obj, str):
        return [obj].__iter__()

    try:
        return obj.__iter__()
    except AttributeErrr:
        return [obj].__iter__()

class MatchRule(Rule):
    """Abstract rule implementation: allows or forbids calls from one
       module or set of modules to another module or set of modules."""
    def __init__(self, mod1, mod2):
        self._mod1 = frozenset(do_iter(mod1))
        self._mod2 = frozenset(do_iter(mod2))

    def apply(self, cg, mod1, mod2):
        inter1 = self._mod1.intersection(mod1.getGroups())
        inter2 = self._mod2.intersection(mod2.getGroups())
        if inter1 and inter2:
            return self._match(cg, inter1, inter2, mod1, mod2)
        else:
            return None

    def _match(self, cg, inter1, inter2, mod1, mod2):
        raise NotImplemented()

class Forbid(MatchRule):
    """Abstract rule implementation: forbids calls from one
       module or set of modules to another module or set of modules."""
    def __init__(self, mod1, mod2):
        super(Forbid, self).__init__(mod1, mod2)
    def _match(self, cg, i1, i2, mod1, mod2):
        return Fail("%s may not call %s"%(i1, i2),mod1, mod2)

class Allow(MatchRule):
    """Abstract rule implementation: allows calls from one
       module or set of modules to another module or set of modules."""
    def __init__(self, mod1, mod2):
        super(Allow, self).__init__(mod1, mod2)
    def _match(self, cg, i1, i2, mod1, mod2):
        return OK("%s may call %s"%(i1, i2), mod1, mod2)

class RuleBuilder(object):
    """Builder object: creates a Rules object in a hopefully nice notation."""
    def __init__(self):
        self._rules = Rules()
        self._order = 0

    def failByDefault(self):
        self._rules.addRule(NothingAllows(), priority=1000000000)

    def succeedByDefault(self):
        self._rules.addRule(ItsAllOkay(), priority=1000000000)

    def allow(self, m1, m2):
        self._rules.addRule(Allow(m1, m2), priority=self._order)
        self._order += 1

    def forbid(self, m1, m2):
        self._rules.addRule(Forbid(m1, m2), priority=self._order)
        self._order += 1

    def getRules(self):
        return self._rules
