#!/usr/bin/python
# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

"""Analyzes C files at a very simple pre-lexical level, looking for
   function definitions (in ctags style), doxygen comments, and movement
   comments.

   We allow \module_group in doxygen comments to declare that modules
   belong to a group.

   We have two kind of magic comments that move things.  One is for moving
   groups of stuff:
      // BEGIN CHOP INTO filename
          ... big block of code.
      // END CHOP INTO filename

   And one is for annotating particular functions to be moved.
     /**
      * Regular doxygen documentation
      * \moveto filename
      */
    void
    foo(...)
    {
      ...
    }

"""

import re
import calltool.Misc
from calltool.CallgraphRewrite import MoveFunction
from logging import warn

__all__ = [ 'chopFile' ]

# Matches a function definition's first line, in the way that Tor
# writes them.
FUNC_DEF = re.compile(r'^([a-zA-Z_][a-zA-Z_0-9]*),?\(')

def lex_base(f):
    """Read the file in f, yielding pairs of (lexeme-type, line).  The
       lexeme types are:
           'MoveTo' -- a \moveto line in a doxygen comment.
           'ModGroup' -- a \module_group line in a doxygen comment.
           'DoxyText' -- a regular part of a doxygen comment.
           'Chop' -- a BEGIN CHOP or END CHOP directive.
           'FuncDef' -- The first line of a function definition
           'EndOfDef' -- The } line at the end of a function definition
           'Code' -- Anything else.
    """
    # kludge-- the lexeme types should probably turn into an OO thing.

    inDoxyComment = False
    begin_chop_re = re.compile(r'^// \s*  BEGIN \s+ CHOP', re.X|re.I)
    end_chop_re = re.compile(r'^// \s* END \s+ CHOP', re.X|re.I)
    for line in f:
        startsDoxyComment = line.startswith("/**")
        endsComment = "*/" in line
        # kludge -- assumes correctness.  Find weird stuff after end
        # of doxygen comment!

        if startsDoxyComment:
            inDoxyComment = True

        if inDoxyComment:
            if endsComment:
                inDoxyComment = False

            if "\moveto" in line:
                yield "MoveTo", line
            elif "\module_group" in line:
                yield "ModGroup", line
            else:
                yield "DoxyText", line

        elif begin_chop_re.match(line):
            yield "Chop", line
        elif end_chop_re.match(line):
            yield "Chop", line

        elif FUNC_DEF.match(line):
            yield "FuncDef", line

        elif line.startswith("}"):
            yield "EndOfDef", line

        else:
            yield "Code", line

class MovementDirective(object):
    """Tells us where to move a chunk."""
    def __init__(self, destination, target=None):
        self._dest = destination
        self._target = target

    @staticmethod
    def fromChopLine(line):
        pass#XXXX

    @staticmethod
    def fromMoveLine(line):
        pass#XXXX

    @staticmethod
    def fromLine(line):
        pass#XXXX

class Chunk(object):
    """A group of lines in the C file."""
    def __init__(self, tp):
        self._type = tp
        self._lines = []
        self._controlLine = None

    def addLine(self, line):
        self._lines.append(line)

    def splitBefore(self, idx):
        """Split this chunk into two sub-chunks, and return them."""
        ch1 = Chunk(self._type, self._lines[:idx])
        ch2 = Chunk(self._type, self._lines[idx:])
        return [ch1,ch2]

    def setControlLine(self, line):
        """Note the line 'line' as telling us what to do with this function."""
        self._controlLine = MoveDirective.fromLine(line)

    def getDeclaredFunctions(self):
        """Return a set of all the functions declared in this chunk."""
        s = frozenset(FUNC_DEF.match(line).group(1)
                        for (tp,line) in self._lines if tp == 'FuncDef')
        return s

    def getMoveTarget(self):
        """Return the file or module into which we're moveing this chunk, or
           None."""
        if self._type == 'Chop':
            return "XXX"#XXX
        elif self._type == 'MoveTo':
            return "XXX"#XXX
        else:
            assert self._type == 'Text'
            return None

class ChunkedFile(object):
    """A set of Chunks representing a C file."""
    def __init__(self, fname):
        self._fname = fname
        self._chunks = []
        self._modGroups = set()

    def setModuleName(self, modname):
        self._modname = modname

    def splitChunkBefore(self, chunk, idx):
        idx = self._chunks.indexof(chunk)
        self[idx:idx+1] = chunk.split(idx)
        return self[idx:idx+1]

    def addChunk(self, ch):
        self._chunks.append(ch)

    def addToCGRewriter(self, rewriter):
        """Tell the CG rewriter in 'rewriter' about all the movements in
           this file."""
        modname = self._modname
        for ch in self._chunks:
            if ch._type == 'Text':
                pass
            else:
                target = ch.getMoveTarget()
                for fn in ch.getDeclaredFunctions():
                    rewriter.add(MoveFunction(fn, target, modname))

    def addToModuleGroup(self, group):
        self._modGroups.add(group)

    def getModuleGroup(self):
        return self._modGroups

def indexed(it):
    """If it yields a,b,c,d,e... then yield (0,a),(1,b),(2,c),(3,d),(4,e)..."""
    c = 0
    for i in it:
        yield c, i
        c += 1

class MovetoError(Exception):
    pass

def find_moveto_range(chunk, moveto_pos):
    """Given a chunk with a \moveto line at 'moveto_pos', find the first
       and last line that should move because of that directive."""
    start = end = None
    # First find the function-ending }.  That's the last line we move.
    for i in calltool.Misc.range_(moveto_pos, len(chunk._lines)):
        tp = chunk._lines[i][0]
        if tp == 'EndOfDef':
            end = i
            break
    # Now back up over every comment line that preceeds the \moveto.
    # They are all getting moved.
    for i in calltool.Misc.range_(moveto_pos, -1, -1):
        tp = chunk._lines[i][0]
        if tp not in ("MoveTo", "ModGroup", "DoxyText"):
            start = i+1
            break
    if start is None or end is None:
        warn("Found a confusing moveto")
        raise MovetoError()
    return start,end

def chopFile(fname):
    """Read the C file in fname and return a new ChunkedFile for it, with
       all movement directives parsed.
    """
    cf = ChunkedFile(fname)
    inChunk = Chunk('Text')
    cf.addChunk(inChunk)

    # First lex...
    lines = [ (tp,line) for tp,line in lex_base(open(fname, calltool.Misc.READ_TEXT)) ]

    # Then find the module groups.
    for tp, line in lines:
        if tp != 'ModGroup':
            continue
        m = re.match(line, r'\\module_group\s+(\S.*)')
        if not m:
            warn("Bad module group line %r in %s",line,fname)
            continue
        for group in m.group(1).split():
            cf.addToModuleGroup(group)

    inChop = False

    # Then find chopped pieces.
    for tp, line in lines:
        if tp == 'BeginChop':
            if inChop:
                warn("Nested BeginChop in %s", fname)
            else:
                inChunk = Chunk("Chop")
                cf.addChunk(inChunk)
                inChunk.setControlLine(line)
            inChunk.append((tp,line))
        elif inChop:
            inChunk.addLine((tp, line))
            if tp == 'EndChop':
                inChunk = Chunk('Text')
                cf.append(inChunk)
                inChop = False
        else:
            if tp == 'EndChop':
                warn("EndChop without BeginChop in %s", fname)
            inChunk.addLine((tp, line))

    # Now for all the text (unchopped pieces), find the MoveTos.
    for chunk in cf._chunks[:]:
        if chunk._type == 'Chop':
            continue
        moveto_pos = [p for p,(tp,l) in indexed(chunk._lines) if tp == 'MoveTo']
        moveto_pos.reverse()
        for pos in moveto_pos:
            control = chunk._lines[pos]
            try:
                start,end = find_moveto_range(chunk, pos)
            except MovetoError:
                continue
            mid,rest = cf.splitChunkBefore(chunk, end+1)
            start,mid = cf.splitChunkBefore(mid, start)
            mid.setControlLine(control)
            mid._type = "MoveTo"

    return cf
